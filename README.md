# fortran
Pages : https://moueza.gitlab.io/fortran/


Import from Google Drive archives https://docs.google.com/document/d/1Mee76BHXxzCrTMGcosR4ffqFgumRJHIB4hDXDkrM9n4/edit?usp=drivesdk


(Inspired by https://github.com/moueza/latex-pdfs/issues/1 )

Lesson : http://www.math.hawaii.edu/~hile/fortran/fort7.htm + big book https://github.com/moueza/isbn-toutes-les-mathematiques-2-782100-047451 p1048 + http://www-perso.unilim.fr/vincent.jalby/eco2math.html

gfortran f90 and Docker (for Gitlab AutoDevOps compile check) : https://urban-institute.medium.com/fortran-and-docker-how-to-combine-legacy-code-with-cutting-edge-components-35e934b15023

