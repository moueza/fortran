
 
module aln
  implicit none   !pas ds contains
contains
 
  function prv2v(r,v)
    ! produit reel*vecteur -> vecteur
    implicit none
    integer, parameter :: dim = 4
    real, intent(in) :: r
    real, dimension(:), intent(in) :: v
    real, dimension(size(v)) :: prv2v
    integer i
    do i=1,size(v)
       prv2v(i)=r*v(i)
    end do
  end function prv2v
  ! comparer avec r*v ou v*r
 
  function pvv2r (u,v)
    ! produit vecteur*vecteur -> reel
    ! size(v) == size(u)
    implicit none
    real, dimension(:), intent(in) :: u,v
    real :: pvv2r
    integer i
    pvv2r = 0
    do i=1,size(u)
       pvv2r = pvv2r + u(i)*v(i)
    end do
  end function pvv2r
  ! comparer avec sum(u*v)
 
 
 
 
 
 
  subroutine aelu(a)
    ! remplace la matrice carree a par sa decomposition lu
    ! attention : aucune verification de la taille du pivot.
    ! size(a,1) == size(a,2)
    implicit none
    real, dimension(:,:), intent(inout) :: a
    real r ! l'inverse du pivot
    integer n,i
    n = size(a,1)
    do i=1,n-1
       r = 1.0/a(i,i)
       a(i+1:n,i) = r*a(i+1:n,i)
       a(i+1:n,i+1:n) = a(i+1:n,i+1:n) - pvv2m(a(i+1:n,i),a(i,i+1:n))
       ! on devrait a la place definir une subroutine pour
       ! a <-- a + r u^T v, orientee colonne, avec ici r=-1
    end do
  end subroutine aelu
 
 
 
  function somm(horiz,vert,min,max)  !!!type
    real ,dimension(4) :: horiz,vert
    integer, intent(in) :: min, max
    real ::  prodScalr=0.
    real  :: somm
    integer :: dim,k
 
    do k=min,max
       prodScalr=prodScalr+horiz(k)*vert(k)   !peut etre system-implementÃ©
    end do
    somm=prodScalr
    !RETURN 
  end  function somm
 
 
 
 
  subroutine getL(a)
    real, intent(inout), dimension(:,:) :: a
    integer ::dim,i,j
    dim=size(a,1)
 
    do j=1,dim  !ds cet ordre
       do i=1,j-1
          a(i,j)=0
       end do
       a(i,i)=1
    end do
  end  subroutine getL
 
  subroutine getU(a)
    real, intent(inout), dimension(:,:) :: a
    integer ::dim,i,j
    dim=size(a,1)
 
    do j=1,dim  !ds cet ordre
       a(j,j)=1
       do i=j+1,dim
          a(i,j)=0
       end do
    end do
  end  subroutine getU
 
 
 
 
  subroutine luxeb(lu,b)
    ! remplace b par la solution y de ly=b,
    ! puis par la solution x de ux = y
    ! l et u sont stockees dans lu qui est carree
    ! de meme tailles que b, et y et x.
    implicit none
    real, dimension(:,:), intent(in) :: lu
    real, dimension(size(lu,1)), intent(inout) :: b
    integer n,i
    n = size(lu,1)
    ! resolution de ly=b par descente
    do i=1,n-1
       b(i+1:n) = b(i+1:n) - lu(i+1:n,i)*b(i)
    end do
    ! resolution de ux=y par remontee
    do i=n,2,-1
       b(i) = b(i)/lu(i,i)
       b(1:i-1) = b(1:i-1) - lu(1:i-1,i)*b(i)
    end do
    b(1) = b(1)/lu(1,1)
  end subroutine luxeb
 
 
 
  function max(vert,minn,maxx,indicMax)
    real, intent(in), dimension(:) :: vert
    real :: mem,max
    ! taill=size(vert,1)
    integer :: i,taill, minn, maxx
    integer , intent(inout) :: indicMax
    mem=vert(minn)
    do i=minn+1,maxx
       if (abs(mem)<abs(vert(i))) then
          mem=vert(i)
          indicMax=i
       endif
    end do
  end function max
 
  subroutine paelu(a,piv)
    !http://ima.epfl.ch/asn/exos02/tableaux_cours2002.pdf
    implicit none
    real ,dimension(:) :: piv
    real, intent(inout), dimension(:,:) :: a
    integer :: i,n,j
    integer  :: indicMaxx
    real r,m
    n=size(a,1)  !hyp : a carree
    print  *, "in paelu"
    do i=1,n-1
 
       m=max(a(i,:),i,n,indicMaxx)
       r=1.0/m
 
       a(i+1:,i)= r*a(i+1:,i)
       !      a(i+1:n,i+1:n)= a(i+1:n,i+1:n)-a(i+1:n,i)*a(i,i+1:n)
       do j=i+1, n
          a(i+1:,j)=a(i+1:,j)+a(i+1:,i)*a(i,j)
       end do
    end  do
  end subroutine paelu
 
 
 
 
 
  subroutine luxepb(lu,piv,b)
    implicit none
    real, dimension(:,:), intent(in) :: lu
    integer, dimension(size(lu,1)-1), intent(in) :: piv
    real, dimension(size(lu,1)), intent(inout) :: b
    integer dim,i
    integer, dimension(size(lu,1)) :: perm
    dim = size(lu,1)
    perm = (/(i,i=1,dim)/)
    do i = 1,dim-1 ! reconstitution de perm application a b
       call iech(perm(i),perm(piv(i)))      
       call rech(b(i),b(piv(i)))
    end do
    ! resolution de ly=b par descente
    do i=1,dim-1
       b(i+1:) = b(i+1:) - lu(perm(i+1:),i)*b(i)
    end do
    ! resolution de ux=y par remontee
    do i=dim,2,-1
       b(i) = b(i)/lu(perm(i),i)
       b(1:i-1) = b(1:i-1) - lu(perm(1:i-1),i)*b(i)
    end do
    b(1) = b(1)/lu(perm(1),1)
  end subroutine luxepb
 
 
 
  subroutine  afficheDiffM(M,N)
    real, intent(inout), dimension(:,:) :: M,N
    integer ::dimi,dimj,i,j
    dimi=size(M,1)
    dimj=size(M,2)
    do i=1,dimi
       print *, (M(i,j)-M(i,j), j=1,dimj)
    end do
  end  subroutine  afficheDiffM
 
 
  subroutine  afficheM(MM)
    real, intent(in), dimension(:,:) :: MM
    integer ::dimi,dimj,i,j
    dimi=size(MM,1)
    dimj=size(MM,2)
    do i=1,dimi
       print *, (MM(i,j), j=1,dimj)
    end do
  end  subroutine  afficheM
 
 
 
  subroutine  afficheL(a)
    real, intent(inout), dimension(:,:) :: a
    integer ::dim,i,j
    dim=size(a,1)
    do i=1,dim !cet ordre car affichage par lignes
       !& edit descriptor evite saut de ligne         
       !:mieux vaut etre imperatif que conditionnel
       print *,(a(i,j), j=1, i-1),1, (0, j=i+1, dim) 
    end do !essayer en + compacte sans do par intervalle
  end subroutine   afficheL
 
 
 
  subroutine  afficheU(a)
    real, intent(inout), dimension(:,:) :: a
    integer ::dim,i,j
    dim=size(a,1)
    !mat carrÃ©e
    !cet ordre car affichage par lignes
 
    ! print *,((0,j=1,i-1) ,1,(a(i,j), j=i+1,dim), i=1, dim)  !!!optimisÃ©
 
    do i=1,dim
       print *, (0,j=1,i-1) ,1,(a(i,j), j=i+1,dim)
    end do
  end subroutine  afficheU
  !---------------------
 
  subroutine luxebTriDiag(tridiagDiag,tridiagSurdiag,b)
    !TD f43
    real ,intent(in),dimension(:)::b
    integer ::i,dim
    real,intent(out),dimension(:)::u
    real,intent(out),dimension(b)::v
 
    dim=size(tridiagDiag)
    u=tridiagDiag
    v=tridiagSurdiag
 
    !montee
    do i=1,dim-1
       u(i)=a(i)  !a(i) v col=diag
       v(i)=b(i)
       c(i)=c(i)/a(i)
       !a(:i+1)=a(i+1)-c(i)*b(i), ms comme triDiag sym + dc stable sans pivotage -> b=c dc:
       a(i+1)=a(i+1)-b(i)*b(i)
       !resolution Ly=B  (Ux=y):
    end do
 
    ! descente:
    luxebTriDiag=a
    do i=dim:-1:2
       B(i)=B(i)/a(i)
       B(i-1)=B(i-1)
    end do
  end subroutine  luxebTriDiag
 
 
  function luxebTriDiagSymq(tridiagDiag,tridiagSurdiag,b)
    !TD f43
    real ,intent(in),dimension(:)::b
    integer ::i,dim
    real,intent(out),dimension(:)::u
    real,intent(out),dimension(b)::v
 
    dim=size(tridiagDiag)
    u=tridiagDiag
    v=tridiagSurdiag
 
    !montee
    do i=1,dim-1
       u(i)=a(i)  !a(i) v col=diag
       v(i)=b(i)
       c(i)=c(i)/a(i)
       !a(:i+1)=a(i+1)-c(i)*b(i), ms comme triDiag sym + dc stable sans pivotage -> b=c dc:
       a(i+1)=a(i+1)-b(i)*b(i)
       !resolution Ly=B  (Ux=y):
    end do
 
    ! descente:
    luxebTriDiag=a
    do i=dim:-1:2
       B(i)=B(i)/a(i)
       B(i-1)=B(i-1)
    end do
 
 
 
    do i=1,n-1
       l(i)=b(i)/d(i)
       d(i+1)=a(i+1) -pow(l(i),2)*d(i)
    end do
 
 
 
    !(a(1)=a(1))facultatif car a1 ne change pas
    !do i=1,n-1
    !b(i)=b(i)/a(i)
    !a(i+1)=a(i+1)-pow(b(i),2)*a(i)
 
 
end do 
end function luxebTriDiagSymq
 
function detTS(diag, surdiag)
real,intent(in),dimension(:)::Diag,
real,intent(in),dimension(:)::Surdiag
integer::dim
dim=size(Diag,1)
 
produi=1
do i=1,dim
    produi=produi*diag(i)    !cf rapport
 
end do
detTS=produi*(diag(dim-1)*diag(dim)-surdiag(dim-1)
 
end function detTS
 
subroutine  Rayleigh(Diag,Surdiag,vpEstim,vectPestim)
real,intent(in),dimension(:)::Diag,
real,intent(in),dimension(:)::Surdiag
integer::dim
real,intent(in):: valPEstim
real,intent(in),dimension(:):: vectPEstim
 
 
dim=size(Diag,1)
 
rk=1/normV2(xk)
eps=1e-6
do        ! while(rk>epsilon)
    !test d arret
    if (detTS(diag, surdiag)=0) then
       print *, error
    else
       if ((abs(vp_min))<eps) then
          !singuliere: vp=0 == abs vp < epsilon    
          do k=1,dim  !pr mettre mu sur la diag
             diag2(k)=diag(k)-µk
          enddo
          !luexeb optimisee pr tridiag ,cf td:  luxebTriDiag
 
          zeros(:,:)=0
          x=luxebTriDiag(diagoTridiag,surdiagTridiag,zeros)
       else
 
          x=luxebTriDiag(diagoTridiag,surdiagTridiag,q_km)
 
          qk=xk/normV2(xk)
          µk=sum (matmul(qDual_km ,q_km) !cf feuille: ici sinon inutilité de le mettre en entree
       endif
 
    endif
    until ((abs(rk))<eps)
  end subroutine  Rayleigh
 
 
end module aln
 
 
 
 
