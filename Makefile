all :
	latex  fortran_f90_rapport_algebre_numerique_puissance_inverse_Rayleigh_Mathematiques.tex 
	dvips   fortran_f90_rapport_algebre_numerique_puissance_inverse_Rayleigh_Mathematiques.dvi
	ps2pdf fortran_f90_rapport_algebre_numerique_puissance_inverse_Rayleigh_Mathematiques.ps

init :
	sudo apt install texlive-base texlive-latex-recommended texlive texlive-latex-extra texlive-full texlive-publishers texlive-science texlive-pstricks texlive-pictures

short:
	pdflatex fortran_f90_rapport_algebre_numerique_puissance_inverse_Rayleigh_MathematiquesRetouched.tex

#https://github.com/moueza/isbn-1-59059-585-8-the-definitive-guide-GCC p57 
hello :
	gfortran helloWorld.f90

fib :
	gfortran fibo.f90
